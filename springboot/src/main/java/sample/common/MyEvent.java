package sample.common;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
//import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
//@Document(collection = "event") // 1
public class MyEvent {
    @Id
    private Long id;    // 2
    private String message;


}