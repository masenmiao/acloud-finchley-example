package sample.service;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import sample.common.Person;

@Service
public class PersonFluxService {

	// 模拟反应式Repository库：ReactiveCrudReposity，如MongoDB，spring.data.mongodb.host=192.168.0.101
	// @Autowired
	// private UserRepository userRepository;
	// 反应式Repository 返回 的是 Mono ,或者 Flux

	// 查询所有
	public Flux<Integer> getPersonList() {
		AtomicInteger count = new AtomicInteger();
		return Flux.create(s -> new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				s.next(count.getAndIncrement());
				if (count.get() == 10) {
					s.complete();
				}
			}
		}, 100, 100));

		/**
		 * 注意：
		 * MongoDB @Tailable仅支持有大小限制的（“capped”）collection，
		 * @Tailable起作用的前提是至少有一条记录
		 * 自动创建的collection是不限制大小的，因此我们需要先手动创建。
		 * Spring Boot提供的CommandLineRunner可以帮助我们实现这一点
		 * MongoOperations提供对MongoDB的操作方法，由Spring注入的mongo实例已经配置好，直接使用即可；
		 * CollectionOptions.empty().size(200).capped()
		 * 参考：
		 * https://github.com/get-set/get-reactive
		 */
	}

	// 保存persons
	public Mono<Void> savePersons(Flux<Person> persons) {
		// return this.myEventRepository.insert(events).then(); // 2

		return Flux.from(persons).flatMap((person) -> {
			System.out.println("P:" + person);return Mono.empty();
		}).then();

	}
}
