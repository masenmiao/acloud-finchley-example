/*
 * Copyright 2012-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sample.webflux.flux;

import javax.annotation.Resource;

import org.reactivestreams.Subscription;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import sample.common.Person;
import sample.service.PersonFluxService;

/**
 * flux测试
 * @author masen
 *
 */

@RestController
public class FluxController {
	
	@Resource
	PersonFluxService personFluxService;

	//返回一个json 流
	//APPLICATION_STREAM_JSON_VALUE , 流方式：产生一个发送一个 （非流方式是，一次性返回）
	@GetMapping(path="/flux",produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
	public Flux<Integer> welcome() {
		return personFluxService.getPersonList().log();
	}

	//接收一个流
	@PostMapping(path = "/persons", consumes = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Mono<Void> persons(@RequestBody Flux<Person> persons) {   // 1
		//APPLICATION_STREAM_JSON_VALUE 参考mogodb的代码
		return Flux.from(persons).log().flatMap((person) -> {
			try {
				persons.onBackpressureDrop();
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("P:" + person);return Mono.empty();
		}).then();//使用then方法表示“忽略数据元素，只返回一个完成信号”。

		//也可以
		//return persons.doOnNext(person -> System.out.println("P:" + person)).then();

		//模拟mongodb
//		return personFluxService.savePersons(persons);

		//这句话不可以
//		persons.subscribe(System.out::println,
//			    System.err::println,
//			    () -> System.out.println("Completed!"),
//			s-> persons.next());


    }

}
