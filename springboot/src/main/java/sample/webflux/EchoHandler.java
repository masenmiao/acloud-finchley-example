/*
 * Copyright 2012-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sample.webflux;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import sample.common.Person;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.concurrent.Callable;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.http.MediaType.APPLICATION_JSON;

/**
 * Router配置 url 方式
 *
 * @author masen
 */
@Component
public class EchoHandler {

    /**
     * 模拟mono返回
     *
     * @param request
     * @return
     */
    public Mono<ServerResponse> echo(ServerRequest request) {
        System.out.println("echo start");
        return ServerResponse.ok().body(Mono.just("Hello World Mono"), String.class);

    }

    /**
     * 查询远程服务，异步非阻塞webclient调用
     *
     * @param request
     * @return
     */
    public Mono<ServerResponse> queryPerson(ServerRequest request) {
        String id = request.pathVariable("id");
        System.out.println("id :" + id);
        WebClient webClient = WebClient.create("http://localhost:8080");
        Mono<ServerResponse> mono = webClient.get().uri("/getPerson/" + id)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .log()
            .flatMap(response -> {
                System.out.println("reponse statudCode : " + response.statusCode().toString());
                return ServerResponse.ok().body(response.bodyToMono(Person.class), Person.class);
            });
        System.out.println("finished.");
        return mono;
    }

    /**
     * 模拟服务器端持续推送:用Flux 定时 1秒钟发送一个mono
     *
     * @param request
     * @return
     */
    public Mono<ServerResponse> sendTimePerSec(ServerRequest request) {
        //TEXT_EVENT_STREAM ： 代表异步流的方式，一个一个到达客户端
        return ServerResponse.ok().contentType(MediaType.TEXT_EVENT_STREAM).body(  // 1
            Flux.interval(Duration.ofSeconds(1)).   // 2
                map(l -> new SimpleDateFormat("HH:mm:ss").format(new Date())),
            String.class);
    }

}
