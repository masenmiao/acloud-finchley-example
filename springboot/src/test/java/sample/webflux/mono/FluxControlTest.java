package sample.webflux.mono;

import java.time.Duration;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.LongAdder;
import org.junit.Test;
import org.reactivestreams.Subscription;
import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.SignalType;
import reactor.core.scheduler.Schedulers;

public class FluxControlTest {

	/**
	 * 可以通过自定义具有流量控制能力的Subscriber进行订阅。
	 * Reactor提供了一个BaseSubscriber，我们可以通过扩展它来定义自己的Subscriber。
	 * subscribe(Subscriber subscriber)
	 * 这才是subscribe方法本尊，前边介绍到的可以接收0~4个函数式接口为参数的subscribe最终都是拼装为这个方法，
	 * 所以按理说前边的subscribe方法才是“变体”
	 */

	@Test
	public void testBackpressure1() throws Exception {
		// .subscribe(System.out::println)这样的订阅的时候，直接发起了一个无限的请求（unbounded request），
		// 就是对于数据流中的元素无论快慢都“照单全收”
		// 接收一个Subscriber为参数，该Subscriber可以进行更加灵活的定义
		// Subscriber就需要通过request(n)的方法来告知上游它的需求速度。
		CountDownLatch countDownLatch = new CountDownLatch(1);

		Flux.range(1, 6)    // 1
			.log()
			.parallel().runOn(Schedulers.parallel())//parallel()默认情况下，轨道个数与CPU核数相
			.log()
			.subscribe(System.out::println);
		//range方法生成的Flux采用的是缓存的回压策略，能够缓存下游暂时来不及处理的元素。
		countDownLatch.await(1, TimeUnit.SECONDS);
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		Flux.range(1, 6)    // 1
			.doOnRequest(n -> System.out.println("Request " + n + " values..."))    // 2
			.subscribe(new BaseSubscriber<Integer>() {  // 3
				@Override
				protected void hookOnSubscribe(Subscription subscription) { // 4
					System.out.println("Subscribed and make a request...");
					request(1); // 5
				}

				@Override
				protected void hookOnNext(Integer value) {  // 6
					try {
						TimeUnit.SECONDS.sleep(1);  // 7
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					System.out.println(Thread.currentThread().getName() +" : "+"Get value [" + value + "]");    // 8
					request(1); // 9
				}
			});
		/**
		 * 1 Flux.range是一个快的Publisher；
		 2 在每次request的时候打印request个数；
		 3 通过重写BaseSubscriber的方法来自定义Subscriber；
		 4 hookOnSubscribe定义在订阅的时候执行的操作；
		 5 订阅时首先向上游请求1个元素；
		 6 hookOnNext定义每次在收到一个元素的时候的操作；
		 7 sleep 1秒钟来模拟慢的Subscriber；
		 8 打印收到的元素；
		 9 每次处理完1个元素后再请求1个。
		 */
	}

	@Test
	public void testBackpressure2() throws Exception {
		/**
		 * range方法生成的Flux采用的是缓存的回压策略，能够缓存下游暂时来不及处理的元素。
		 */
		CountDownLatch countDownLatch = new CountDownLatch(1);
		Flux.range(1, 6)
//			.log()// 1
			.doOnRequest(n -> System.out.println("Request " + n + " values..."))    // 2
			.subscribeOn(Schedulers.newParallel("aaa",1))
			.log()
			.publishOn(Schedulers.newParallel("bbb",1))
			.log()

			.subscribe(new BaseSubscriber<Integer>() {  // 3
				@Override
				protected void hookOnSubscribe(Subscription subscription) { // 4
					System.out.println("Subscribed and make a request...");
					request(1); // 5
				}

				@Override
				protected void hookOnNext(Integer value) {  // 6
					try {
						TimeUnit.SECONDS.sleep(1);  // 7
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					System.out.println("Get value [" + value + "]");    // 8
					request(1); // 9
				}
			});
		countDownLatch.await(10, TimeUnit.SECONDS);
	}


	@Test
	public void testBackpressure3() throws Exception {
		// .subscribe(System.out::println)这样的订阅的时候，直接发起了一个无限的请求（unbounded request），
		// 就是对于数据流中的元素无论快慢都“照单全收”
		// 接收一个Subscriber为参数，该Subscriber可以进行更加灵活的定义
		// Subscriber就需要通过request(n)的方法来告知上游它的需求速度。
		CountDownLatch countDownLatch = new CountDownLatch(1);
		
		Flux.range(1, 100)    // 1
//		.log()
//        .doOnRequest(n -> System.out.println("Request " + n + " values..."))    // 2
//			.publishOn(Schedulers.newParallel("aaa",2))
//			.log()
//		.subscribeOn(Schedulers.newParallel("bbb",2))
//			.log()
			.parallel(6).runOn(Schedulers.parallel())//parallel()默认情况下，轨道个数与CPU核数相
			.log()
			.subscribe(new BaseSubscriber<Integer>() {  // 3
				@Override
				protected void hookOnSubscribe(Subscription subscription) { // 4
					System.out.println("Subscribed and make a request...");
					request(2); // 5
				}

				@Override
				protected void hookOnNext(Integer value) {  // 6
					try {
						TimeUnit.SECONDS.sleep(1);  // 7
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					System.out.println(Thread.currentThread().getName() +" : "+"Get value [" + value + "]");    // 8
					request(2); // 9
				}
			});
		//range方法生成的Flux采用的是缓存的回压策略，能够缓存下游暂时来不及处理的元素。
		countDownLatch.await(20, TimeUnit.SECONDS);


	}
	
}
