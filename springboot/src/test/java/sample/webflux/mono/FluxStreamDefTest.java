package sample.webflux.mono;

import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import org.junit.Test;
import org.reactivestreams.Subscription;
import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

public class FluxStreamDefTest {

    @Test
    public void testGenerate1() {
        final AtomicInteger count = new AtomicInteger(1);   // 1 用于计数；

        Flux.generate(sink -> {
            sink.next(count.get() + " : " + new Date());   // 2 向“池子”放自定义的数据；
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (count.getAndIncrement() >= 5) {
                sink.complete();     // 3 告诉generate方法，自定义数据已发完；
            }
        }).subscribe(System.out::println);  // 4 触发数据流。
    }

    @Test
    public void testGenerate2() {
        Flux.generate(
            () -> 1,    // 1 初始化状态值；
            (count, sink) -> {      // 2 第二个参数是BiFunction，输入为状态和sink；
                sink.next(count + " : " + new Date());
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (count >= 5) {
                    sink.complete();
                }
                return count + 1;   // 3 每次循环都要返回新的状态值给下次使用。
            }).subscribe(System.out::println);

        Flux.generate(
            () -> 1,
            (count, sink) -> {
                sink.next(count + " : " + new Date());
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (count >= 5) {
                    sink.complete();
                }
                return count + 1;
            }, System.out::println)     // 1 最后将count值打印出来。这个Consumer在数据流发完后执行,进行清理的资源
            .subscribe(System.out::println);
    }

    /**
     * Push model
     * 创建一个监听器注册到事件源，这个监听器再收到事件回调的时候通过
     * Flux.create的sink将一系列事件转换成异步的事件流：
     */
    @Test
    public void testCreate() throws InterruptedException {
        MyEventSource eventSource = new MyEventSource();    // 1
        Flux.create(sink -> {
                eventSource.register(new MyEventListener() {    // 2
                    @Override
                    public void onNewEvent(MyEventSource.MyEvent event) {
                        sink.next(event);       // 3
                    }

                    @Override
                    public void onEventStopped() {
                        sink.complete();        // 4
                    }
                });
            }
        ).subscribe(System.out::println);       // 5

        for (int i = 0; i < 20; i++) {  // 6
            Random random = new Random();
            TimeUnit.MILLISECONDS.sleep(random.nextInt(1000));
            eventSource.newEvent(new MyEventSource.MyEvent(new Date(), "Event-" + i));
        }
        eventSource.eventStopped(); // 7
    }

    /**
     * Hybrid push/pull model
     * https://blog.csdn.net/get_set/article/details/79549401
     * http://projectreactor.io/docs/core/release/reference/
     */
    @Test
    public void testCreate2() throws InterruptedException {
        MyEventSource eventSource = new MyEventSource();    // 1
//        Flux<String> bridge = Flux.create(sink -> {
//            myMessageProcessor.register(
//                new MyMessageListener<String>() {
//
//                    public void onMessage(List<String> messages) {
//                        for(String s : messages) {
//                            sink.next(s);   // 1
//                        }
//                    }
//                });
//            sink.onRequest(n -> {   // 2 在下游发出请求时被调用；
//                List<String> messages = myMessageProcessor.request(n);  // 3
//                for(String s : message) {
//                    sink.next(s);
//                }
//            });
//            }
//        ).subscribe(System.out::println);       // 5

    }



	
}
