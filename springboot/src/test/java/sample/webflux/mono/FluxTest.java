package sample.webflux.mono;

import java.time.Duration;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;
import sample.common.Person;

public class FluxTest {

	@Test
	public void fluxTest1() {
		// 只有subscribe()方法调用的时候才会触发数据流
		Flux.just(1, 2, 3, 4, 5, 6).subscribe(System.out::println, System.err::println,
				() -> System.out.println("Completed!"));


		//Flux.just(1, 2, 3, 4, 5, 6).flatMap(num -> {
		//	System.out.println("P:" + num);return Mono.just(num*num);
		//}).subscribe(System.out::println);
	}

	@Test
	public void fluxTest2() throws Exception {
		// StepVerifier调试工具 , verify() 才会触发运行
		StepVerifier.create(Flux.just(1, 2, 3, 4, 5, 6))
			.expectNext(1, 2, 3, 4, 5, 6)
			.expectComplete().verify();


	}

	@Test
	public void fluxTest3() throws Exception {
		// StepVerifier调试工具 , verify() 才会触发运行
		StepVerifier.create(Flux.just(1, 2, 3, 4, 5, 6))
			.expectNext(1, 2, 3, 4, 5, 6)
			.expectComplete().verify();
		// StepVerifier.create(generateMonoWithError()).expectErrorMessage("some
		// error").verify();

		// 操作符
		// map 元素映射为新元素
		StepVerifier.create(Flux.range(1, 6) // 1
				.map(i -> i * i)) // 2
				.expectNext(1, 4, 9, 16, 25, 36) // 3
				.expectComplete().verify(); // 4

		// flatMap - 元素映射为流
		StepVerifier.create(Flux.just("flux", "mono").flatMap(s -> Flux.fromArray(s.split("\\s*")) // 1
				.delayElements(Duration.ofMillis(100))) // 2
				.doOnNext(System.out::print)) // 3	注doOnNext方法是“偷窥式”的方法，不会消费数据流
				.expectNextCount(8) // 4
				.verifyComplete();

		//filter - 过滤
		StepVerifier.create(Flux.range(1, 6)
	            .filter(i -> i % 2 == 1)    // 1
	            .map(i -> i * i))
	            .expectNext(1, 9, 25)   // 2
	            .verifyComplete();

		//zip - 一对一合并
		String desc = "Zip two sources together, that is to say wait for all the sources to emit one element and combine these elements once into a Tuple2.";

		CountDownLatch countDownLatch = new CountDownLatch(1);  // 2
	    Flux.zip(
	    		Flux.fromArray(desc.split("\\s+")),
	            Flux.interval(Duration.ofMillis(200)))  // 3	zip之后的流中元素类型为Tuple2
	            .subscribe(t -> System.out.println(t.getT1()), null, countDownLatch::countDown);    // 4
	    countDownLatch.await(10, TimeUnit.SECONDS);     // 5


	}

}
