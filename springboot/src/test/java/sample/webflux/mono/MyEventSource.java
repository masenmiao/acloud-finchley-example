package sample.webflux.mono;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class MyEventSource {

    private List<MyEventListener> listeners;

    public MyEventSource() {
        this.listeners = new ArrayList<>();
    }

    public void register(MyEventListener listener) {    // 1 注册监听器；
        listeners.add(listener);
    }

    public void newEvent(MyEvent event) {
        for (MyEventListener listener :
            listeners) {
            listener.onNewEvent(event);     // 2 向监听器发出新事件；
        }
    }

    public void eventStopped() {
        for (MyEventListener listener :
            listeners) {
            listener.onEventStopped();      // 3 告诉监听器事件源已停止；
        }
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class MyEvent {   // 4 事件类，使用了lombok注解。
        private Date timeStemp;
        private String message;
    }
}
