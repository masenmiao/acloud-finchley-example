package sample.webflux.mono;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;
import reactor.core.scheduler.Schedulers;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class FluePressureRateTest {

    Flux timerPublisher = null;


    @Before
    public void beforeEach() {
        // initialize publisher
        AtomicInteger count = new AtomicInteger();
        timerPublisher = Flux.create(s ->
                        new Timer("publisher").schedule(new TimerTask() {
                            @Override
                            public void run() {
                                s.next(count.getAndIncrement());
                                if (count.get() == 10) {
                                    s.complete();
                                }
                            }
                        }, 100, 100)
                , FluxSink.OverflowStrategy.DROP);
    }

    @Test
    public void testNormal() throws Exception {
        CountDownLatch latch = new CountDownLatch(1);
        timerPublisher
                .subscribe(r -> System.out.println("Continuous consuming " + r),
                        e -> latch.countDown(),
                        latch::countDown);
        latch.await();
    }

    @Test
    public void testBackpressure() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        AtomicReference<Subscription> timerSubscription = new AtomicReference<>();
        Subscriber<Integer> subscriber = new BaseSubscriber<Integer>() {
            @Override
            protected void hookOnSubscribe(Subscription subscription) {
//            	super.hookOnSubscribe(subscription);
                timerSubscription.set(subscription);
                //调用request()方法表示初始化工作已经完成
                //调用request()方法，会立即触发onNext()方法
                System.out.println("subscribe " + Thread.currentThread().getName());
//                subscription.request(1);//只取2个元素
//                subscription.request(Long.MAX_VALUE); //默认取接近无限
            }

            @Override
            protected void hookOnNext(Integer value) {
                System.out.println("consuming " + Thread.currentThread().getName() + " : " +value);
//                try {
//                    Thread.sleep(1000L);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                request(1);
            }

            @Override
            protected void hookOnComplete() {
                System.out.println("完成");
                latch.countDown();
            }

            @Override
            protected void hookOnError(Throwable throwable) {
                latch.countDown();
            }
        };
        //.onBackpressureDrop() 改变publicher 默认配置的流控策略
        timerPublisher.onBackpressureDrop().subscribe(subscriber);//consumer 可以改变回压策略
        new Timer("subscriber").schedule(new TimerTask() {
            AtomicInteger count = new AtomicInteger();
            @Override
            public void run() {
                if(count.getAndIncrement()==5){
                    System.out.println("timerPublisher.onBackpressureDrop");
                    timerPublisher.onBackpressureBuffer();//改变策略，不起作用
                }
				System.out.println("request " + Thread.currentThread().getName() );
                timerSubscription.get().request(2);
            }
        }, 100, 200);
        latch.await();
    }
    /**
     * 说明：上面的例子
     * flux 100ms发一个，Subscriber每200ms要一个，所以，触发了流控
     */

    //consumer 可以改变回压策略
    //consumer 控制流速的例子，上面的 外部 timerSubscription控制

    /**
     * 当订阅者处理慢于发布者时，发布者的按照策略进行流量控制
     * 订阅者订阅时可以指定流控策略，可以覆盖发布者设置的流控策略
     *
     *
     * 通过外部根据业务处理的速率，获得使用Subscription 的 request方法可以控制速率
     * subscrible	要全部		没有流控
     * 外部Subscription控制	可以流控
     *
     *
     * 响应式流模型在pull模型和push模型流处理机制之间动态切换。
     * 当订阅者处理较慢时，它使用pull模型，当订阅者处理更快时使用push模型？
     * P慢			push
     * P快			pull
     * 怎么验证
     */
}
