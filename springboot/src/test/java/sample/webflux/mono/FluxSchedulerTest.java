package sample.webflux.mono;

import java.time.Duration;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.LongAdder;

import org.junit.Test;
import org.reactivestreams.Subscription;

import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.SignalType;
import reactor.core.scheduler.Schedulers;

public class FluxSchedulerTest {

	@Test
	public void schedulerTest1() throws Exception {
		CountDownLatch countDownLatch = new CountDownLatch(1);
		/**
		 * Schedulers.elastic()能够方便地给一个阻塞的任务分配专门的线程
		 */
		Mono.fromCallable(() -> {
			System.out.println("mono create...");
			TimeUnit.SECONDS.sleep(2);
			return " hello world";
		}) // 1
				.subscribeOn(Schedulers.elastic()) // 2	使用subscribeOn将任务调度到Schedulers内置的弹性线程池执行
				.subscribe(System.out::println, null, countDownLatch::countDown);
		countDownLatch.await(10, TimeUnit.SECONDS);
		System.out.println("finished...");
	}
	
	@Test
	public void schedulerTest2() throws Exception {
		
		Flux.range(1, 6).subscribe(System.out::println, System.err::println);

		Flux.range(1, 6)
		.log()
        .map(i -> 10/(i-3)) // 1	当i为3时会导致异常，会终止
        .map(i -> i*i)
        .subscribe(System.out::println, System.err::println);

		System.out.println("..............");
		//	捕获并返回一个静态的缺省值
		Flux.range(1, 6)
			.log()
			.map(i -> 10/(i-3))
			.onErrorReturn(0)   // 1	3出错返回0，之后没有执行
			.map(i -> i*i)
			.subscribe(System.out::println, System.err::println);
		System.out.println("..............");
		//	捕获并执行一个异常处理方法或计算一个候补值来顶替
		Flux.range(1, 6)
			.map(i -> 10/(i-3))
			.onErrorResume(e -> Mono.just(new Random().nextInt(6))) // 提供新的数据流, 3之后没有执行
			.map(i -> i*i)
			.subscribe(System.out::println, System.err::println);

		System.out.println("..............");
		//	捕获，并再包装为某一个业务相关的异常，然后再抛出业务异常
		Flux.just("timeout1")
			.flatMap(k -> {throw new NullPointerException();})   // 1
			.onErrorMap(original -> new RuntimeException("SLA exceeded", original))// 2
			.subscribe(System.out::println, System.err::println);

		System.out.println("捕获，记录错误日志，然后继续抛出 或 值");
		//	捕获，记录错误日志，然后继续抛出
		//doOnError 记录错误日志 onErrorResume 返回另外一个值
		Flux.just("endpoint1", "endpoint2")
			.flatMap(k -> {throw new NullPointerException();})
			.doOnError(e -> {   // 1
				System.out.println("uh oh, falling back, service failed for key " + e);    // 2
			})
			.onErrorResume(e -> {return Mono.just(new Random().nextInt(6));}) //.onErrorResume(e -> getFromCache(k));如果外部服务异常，则从缓存中取值代替。
			.subscribe(System.out::println, System.err::println);

		System.out.println("捕获，catch 处理与降级");
		//?不可以修复其中一个值，让流不cannel吗？
		//或者是否逻辑中进行catch 处理与降级
		Flux.range(1, 6)
			.map(i -> {
				try{return 10/(i-3);}catch(Exception ex){return 1;} // 可以写Static方法，用来写try,catch和逻辑，这里调用
			})
			.onErrorResume(e -> Mono.just(new Random().nextInt(6))) // 提供新的数据流, 3之后没有执行
			.map(i -> i*i)
			.subscribe(System.out::println, System.err::println);
	}
	
	@Test
	public void schedulerTest3() throws Exception {

		//	用 finally 来清理资源，或使用 Java 7 引入的 “try-with-resource”
//		Flux.using(
//		        () -> getResource(),    // 1第一个参数获取资源；
//		        resource -> Flux.just(resource.getAll()),   // 2第二个参数利用资源生成数据流；
//		        MyResource::clean   // 3第三个参数最终清理资源。	
//		);
		
		//doFinally在序列终止（无论是 onComplete、onError还是取消）的时候被执行， 并且能够判断是什么类型的终止事件（完成、错误还是取消），以便进行针对性的清理。
		
		LongAdder statsCancel = new LongAdder();    // 1	用LongAdder进行统计；
		Flux<String> flux =
		Flux.just("foo", "bar")
		    .doFinally(type -> {
		        if (type == SignalType.CANCEL)  // 2	doFinally用SignalType检查了终止信号的类型；
					System.out.println(type);
		          statsCancel.increment();  // 3	如果是取消，那么统计数据自增；
		    }).take(1);  // 4	能够在发出1个元素后取消流。

		flux.subscribe(System.out::println, System.err::println);
		System.out.println(statsCancel);

	}
	@Test
	public void schedulerTest4() throws Exception {
		/**
		 * 重试 , retry不过是再一次从新订阅了原始的数据流，从1开始。第二次，由于异常再次出现，便将异常传递到下游了。
		 */

		Flux.range(1, 6)
			.map(i -> 10 / (3 - i))
			.retry(1)
			.subscribe(System.out::println, System.err::println);
		Thread.sleep(100);  // 确保序列执行完

	}

	@Test
	public void testScheduling() {
		/**
		 * 只保留这个log()的话，可以看到，源头数据流是执行在myElastic-x线程上的；
		 只保留这个log()的话，可以看到，publishOn之后数据流是执行在myParallel-x线程上的；
		 只保留这个log()的话，可以看到，subscribeOn之后数据流依然是执行在myParallel-x线程上的。
		 */
		/**
		 * publishOn会影响链中其后的操作符，比如第一个publishOn调整调度器为elastic，则filter的处理操作是在弹性线程池中执行的；同理，flatMap是执行在固定大小的parallel线程池中的；
		 subscribeOn无论出现在什么位置，都只影响源头的执行环境，也就是range方法是执行在单线程中的，直至被第一个publishOn切换调度器之前，所以range后的map也在单线程中执行。
		 */
		Flux.range(0, 6)
                .log()    // 1
			.publishOn(Schedulers.newParallel("myParallel"))
                .log()    // 2
			.subscribeOn(Schedulers.newElastic("myElastic"))
			.log()    // 3
			.blockLast();
	}

	@Test
	public void testDelayElements() {
		//Flux.delayElements(Duration) 使用的是 Schedulers.parallel()调度器对象
		Flux.range(0, 10)
			.delayElements(Duration.ofMillis(1000))
			.log()
			.blockLast();
	}

	@Test
	public void testParallelFlux() throws InterruptedException {
		/**
		 * 对于一些能够在一个线程中顺序处理的任务，即使调度到ParallelScheduler上，通常也只由一个Worker来执行
		 */
//		Flux.range(1, 10)
//			.publishOn(Schedulers.parallel()) //仅仅异步用了parallel 1个线程，但是没有并发更多线程
//			.log().subscribe();
//		TimeUnit.MILLISECONDS.sleep(10);

		/**
		 * 对任何Flux使用parallel()操作符来得到一个ParallelFlux。不过这个操作符本身并不会进行并行处理，
		 * 而只是将负载划分到多个执行“轨道”上（默认情况下，轨道个数与CPU核数相等）
		 * 为了配置ParallelFlux如何并行地执行每一个轨道，需要使用runOn(Scheduler)
		 * 各个元素的onNext “均匀”分布执行在两个线程上，最后每个线程上有独立的onComplete事件，
		 * 这与publishOn调度到ParallelScheduler上的情况是不同的。
		 */
		Flux.range(1, 10)
			.parallel(2)//2个线程
			.runOn(Schedulers.parallel())
			.log()
			.subscribe(s ->
			{
				System.out.println(s);
				try {
					Thread.sleep(1000L);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			});

		TimeUnit.MILLISECONDS.sleep(6000);
	}

	@Test
	public void testBackpressure() throws InterruptedException {
		Flux.range(1, 6)
			.log()// 1
			.doOnRequest(n -> System.out.println("Request " + n + " values..."))    // 2
			.subscribe(new BaseSubscriber<Integer>() {  // 3
				@Override
				protected void hookOnSubscribe(Subscription subscription) { // 4
					System.out.println("Subscribed and make a request...");
					request(1); // 5
				}

				@Override
				protected void hookOnNext(Integer value) {  // 6
					try {
						TimeUnit.SECONDS.sleep(1);  // 7 限流
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					System.out.println("Get value [" + value + "]");    // 8
					request(1); // 9
				}
			});
		TimeUnit.MILLISECONDS.sleep(10000);
	}

	@Test
	public void testBackpressure2() throws Exception {
		// .subscribe(System.out::println)这样的订阅的时候，直接发起了一个无限的请求（unbounded request），就是对于数据流中的元素无论快慢都“照单全收”
		// 接收一个Subscriber为参数，该Subscriber可以进行更加灵活的定义
		// Subscriber就需要通过request(n)的方法来告知上游它的需求速度。
		CountDownLatch countDownLatch = new CountDownLatch(1);
		
		Flux.range(1, 6)    // 1
		.log()

//        .doOnRequest(n -> System.out.println("Request " + n + " values..."))    // 2
//			.publishOn(Schedulers.newParallel("aaa",3))
//			.log()
//		.subscribeOn(Schedulers.newParallel("bbb",3))
//			.log()
			.parallel().runOn(Schedulers.parallel())//parallel()默认情况下，轨道个数与CPU核数相
			.log()
        .subscribe(new BaseSubscriber<Integer>() {  // 3通过重写BaseSubscriber的方法来自定义Subscriber；
            @Override
				protected void hookOnSubscribe(Subscription subscription) { // 4订阅的时候执行的操作；
                System.out.println("Subscribed and make a request...");
                request(3); // 5订阅时首先向上游请求1个元素；
            }

            @Override
            protected void hookOnNext(Integer value) {  // 6每次在收到一个元素的时候的操作；
				System.out.println("Get value [" + value + "] start.");    // 8
                try {
                    TimeUnit.SECONDS.sleep(1);  // 7 模拟慢的Subscriber；
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Get value [" + value + "] end.");    // 8
                request(3); // 9每次处理完1个元素后再请求1个
            }
        });
		//range方法生成的Flux采用的是缓存的回压策略，能够缓存下游暂时来不及处理的元素。
		countDownLatch.await(10, TimeUnit.SECONDS);

		//上面怎么并发3处理呢
	}
	
}
