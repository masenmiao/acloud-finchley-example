package sample.webflux.mono;

import java.time.Duration;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import sample.common.Person;

public class FluxSteamTest {

    /**
     * 1这次我们使用WebClientBuilder来构建WebClient对象； 2配置请求Header：Content-Type: application/stream+json；
     * 3获取response信息，返回值为ClientResponse，retrive()可以看做是exchange()方法的“快捷版”； 4使用flatMap来将ClientResponse映射为Flux；
     * 5只读地peek每个元素，然后打印出来，它并不是subscribe，所以不会触发流； 6上个例子中sleep的方式有点low，blockLast方法，顾名思义，在收到最后一个元素前会阻塞，响应式业务场景中慎用。
     */
    @Test
    public void webClientReturnFlux() throws InterruptedException {
        WebClient webClient = WebClient.builder().baseUrl("http://localhost:8080").build(); // 1
        webClient.get().uri("/flux").accept(MediaType.APPLICATION_STREAM_JSON) // 2
            .exchange() // 3
            .flatMapMany(response -> response.bodyToFlux(Integer.class)) // 4 无法用bodyToMono
            .log()
            .doOnNext(System.out::println) // 5
            .blockLast(); // 6
    }

    /**
     * 1.声明速度为每秒一个Person元素的数据流，不加take的话表示无限个元素的数据流； 2.声明请求体的数据格式为application/stream+json； 3.body方法设置请求体的数据。
     */
    @Test
    public void webClientSendFlux() {
        Flux<Person> persons = Flux.interval(Duration.ofSeconds(1))
            .map(l -> new Person(System.currentTimeMillis(), "message-" + l))
            .take(5)
            .log(); // 1
        WebClient webClient = WebClient.create("http://localhost:8080");

        webClient.post().uri("/persons").contentType(MediaType.APPLICATION_STREAM_JSON) // 2
            .body(persons, Person.class)  // 3 body方法设置请求体的数据
            .retrieve().bodyToMono(Void.class)
            //.log()
            .block();
    }

}
