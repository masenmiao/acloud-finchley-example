package sample.webflux.mono;

import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebFlux;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Mono;
import sample.SampleWebFluxApplication;

// @Runwith是JUnit标准的一个注解，Spring的单元测试都用SpringRunner.class
@RunWith(SpringRunner.class)
// @SpringBootTest用于Spring Boot应用测试，默认根据包名逐级往上寻找应用启动类
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)

public class MonoTest {
    /**
     * 1创建WebClient对象并指定baseUrl；
     * 2HTTP GET； 3异步地获取response信息；
     * 4将response body解析为字符串；
     * 5打印出来；
     * 6由于是异步的，我们将测试线程sleep 1秒确保拿到response，也可以像前边的例子一样用CountDownLatch。
     */
    @Test
    public void webClientTest1() throws InterruptedException {
        WebClient webClient = WebClient.create("http://localhost:8080"); // 1
        Mono<String> resp = webClient.get().uri("/") // 2
            .retrieve() // 3
            .bodyToMono(String.class);
        resp.subscribe(System.out::println); // 5
        TimeUnit.SECONDS.sleep(1); // 6
    }

    @Test
    public void webClientTest2() throws InterruptedException {
        System.out.println(Thread.currentThread().getName() + "开始测试");
        WebClient webClient = WebClient.create("http://localhost:8080"); // 1
        Mono<String> resp = webClient.get().uri("/") // 2
            .retrieve() // 3
            .bodyToMono(String.class)
            .log()
            .map(result -> {
                System.out.println(Thread.currentThread().getName() + "开始转换");
                return result + " every one";}); // 4

        System.out.println("订阅");
        resp.subscribe(System.out::println); // 5

        System.out.println(Thread.currentThread().getName() + "测试结束");

        TimeUnit.SECONDS.sleep(1); // 6
    }


    @Test
    public void webClientTest3() throws InterruptedException {
        WebClient webClient = WebClient.create("http://localhost:8080"); // 1
        Mono<String> resp = webClient.get().uri("/") // 2
            .retrieve() // 3
            .bodyToMono(String.class)
            //.log()
            ; // 4
        System.out.println("订阅");

        resp.subscribe(
            // 下一个
            result -> System.out.println("consumer : " + result),
            // 异常时
            exception -> System.out.println("Error : " + exception.getMessage()),
            // 完成
            () -> System.out.println("Completed!"),
            // 订阅时
            subscription -> {
                System.out.println("the consumer to invoke on subscribe signal, to be used");
                subscription.request(1);
            }
        ); // 5
        TimeUnit.SECONDS.sleep(5); // 6
    }

}
